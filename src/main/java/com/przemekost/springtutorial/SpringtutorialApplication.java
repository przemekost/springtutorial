package com.przemekost.springtutorial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringtutorialApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringtutorialApplication.class, args);
		System.out.println("hello");
		System.out.println("developer branch");
		System.out.println("pull request");

	}
}
